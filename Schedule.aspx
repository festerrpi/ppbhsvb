﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Schedule.aspx.vb" Inherits="Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Current Match Schedule
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:Panel ID="Panel1" runat="server">
        <p><b>Fall 2021 Schedule</b> &nbsp; &nbsp; (<a href="https://www.shoreconferencenj.org/g5-bin/client.cgi?cwellOnly=1&G5statusflag=view&schoolname=&school_id=32&G5button=13&G5genie=249&vw_schoolyear=1&vw_agl=337-3-4,337-3-5,&manual_access=1" target="_blank">full schedule with match times</a>) (<a href="https://highschoolsports.nj.com/school/pt-pleasant-bch-point-pleasant-beach/girlsvolleyball/season/2021-2022" target="_blank">NJ.com schedule with results</a>)<br /><br />
        8/25 - Manasquan (Away - Scrimmage)<br />
        8/27 - Brick Memorial (Home - Scrimmage) arrive at school 8:30<br />
        8/30 - Central Regional (Away - Scrimmage) arrive at school 7:00<br />
        <span style="text-decoration:line-through;">9/2 - Barnegat (Away - Scrimmage)</span> cancelled<br />
        9/7 - Holmdel (Away)<br />
        9/8 - Lakewood (Home)<br />
        9/9 - Lacey (Away)<br />
            9/11 - Brick Memorial Tournament - Varsity (Away)<br />
        9/13 - Trinity Hall (Away)<br />
            9/14 - Red Bank Regional (Away)<br />
        9/15 - Asbury Park (Away) - Varsity only<br />
        9/17 - Keyport (Home)<br />
        9/20 - Manasquan (Home)<br />
        9/21 - St. Rose (Away)<br />
        9/23 - Mater Dei (Home)<br />
        9/28 - Middletown South (Away)<br />
        9/29 - Trinity Hall (Home)<br />
        10/1 - Asbury Park (Home)<br />
        10/2 - Barnegat (Home)<br />
            10/4 - Red Bank Regional (Home)<br />
        10/5 - Keyport (Away)<br />
        10/7 - Manchester (Home)<br />
        10/8 - Freehold Boro (Home)<br />
            10/11 - Toms River South (Away) *** Added 9/29<br />
        10/12 - St. Rose (Home)<br />
        10/13 - Mater Dei (Away)<br />
        10/14 - <span class="done">Long Branch (Away)</span> *** cancelled - no bus!<br />
            10/16 - Brick Memorial Tournament - Freshmen (Away)<br />
            10/18 - Freehold Boro - Shore Conference Tournament (Away)<br />
        10/19 - Jackson Memorial (Away)<br />
            10/20 - Toms River East - Shore Conference Tournament (Away)<br />
        10/21 - Brick Township (Away) *** rescheduled from 10/20<br />
        10/22 - Middletown North (Home)<br />
            10/23 - Brick Memorial Tournament - JV (Away)<br />
            10/27 - Marlboro (Away) *** added 10/20<br />
            10/28 - Wall (Away) *** added 10/26<br />
            11/1 - Johnson - State Tournament (Home)<br />
            11/4 - Florence - State Tournament (Home)<br />
            11/9 - Delaware Valley - State Tournament (Away)<br />
        </p>

    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server">
        <p><b>Spring 2021 Schedule</b> with Varsity results (and other notes)<br />
        <p><span class="done">3/11 - Jackson Memorial (Home - Scrimmage)</span></p>
        <p><span class="done">3/16 - Brick Township (Home - Freshmen only)</span> (Win!)<br />
           <span class="done">3/18 - St. Rose (Home)</span> Win 2-0<br />
           <span class="done">3/19 - Colts Neck (Away - JV only)</span> (Loss)<br />
           <span class="done">3/20 - Lakewood (Away)</span> Loss 2-1<br />
           <span class="done">3/22 - Asbury Park (Home)</span> Win 2-0<br />
           <span class="done">3/24 - Asbury Park (Away)</span> Win 2-1<br />
           <span class="done">4/7 - Manasquan (Away)</span> Loss 2-0<br />
           <span class="done">4/9 - Mater Dei (Away)</span> Win 2-0<br />
           <span class="done">4/13 - Mater Dei (Home)</span> Win 2-0<br />
           <span class="done">4/15 - Keyport (Away)</span> Win 2-1<br />
           <span class="done">4/16 - Keyport (Home)</span> Win 2-0 SENIOR NIGHT!<br />
           <span class="done">4/20 - Trinity Hall (Away)</span> Loss 2-1<br /><br />
        </p>       
        </asp:Panel>
</asp:Content>