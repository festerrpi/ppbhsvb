﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Vocabulary.aspx.vb" Inherits="Vocabulary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Volleyball Vocabulary
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="Panel1" runat="server">Here are a bunch of words that you may hear on the court.  It's important to know what they mean!  Check back for updates - this is only a sample.</asp:Panel>
<br />
    <asp:Panel ID="Panel2" runat="server">
        <asp:Label ID="lblWordList" runat="server" Text="" OnLoad="Page_Load"></asp:Label>
    </asp:Panel>
</asp:Content>

