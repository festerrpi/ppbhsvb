﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="ICYMI.aspx.vb" Inherits="ICYMI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">In Case You Missed It (Practice)
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <asp:Panel ID="Panel1" runat="server"><u>Summer Week 1</u><br />
        <ul>
            <li>Knee and ankle strength/injury prevention</li>
            <li>Basic jumping workout</li>
            <li>Proper swing technique, control, and accuracy.</li>
            <li>Cut shots - pinkie down and thumb down</li>
            <li>Everybody sets, sometimes!</li>
            <li>Pass, set, hit</li>
            <li>Competitive Queens</li>
            <li>USA drill</li>
        </ul>
        </asp:Panel>
</asp:Content>
