﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Information.aspx.vb" Inherits="Information" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Information
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="Panel1" runat="server">
    <a href="https://youtu.be/1EnBBqj1WZI">NJSIAA Return To Play (Video 9:47)</a>
    </asp:Panel>
    <br />
    <asp:Panel ID="Panel2" runat="server"><b>Summer Practice Schedule</b><br />
        <br />
        First day of practice TBD<br />
        Mondays - Fridays : Gym opens at 7 a.m.  Warmup laps begin at 7:30 - don't be late!  Practice until 10:30.<br /><br />
        <b>Official Practices</b><br /><br />
        Once official practices begin, you need to attend 6 practices before participating in a scrimmage.  Please plan accordingly!<br />
<!--        We had a team meeting Friday, February 12th.&nbsp; If you missed the meeting, please get in touch with a coach or a captain asap.<br /> <span style="text-decoration: underline">Most important points covered</span>
    <ul style="margin-top:0px;">
        <li>get your physical done and submitted in time to have it approved</li>
        <li>if you haven't already done it, you need to complete IMPACT testing</li>
        <li>fill out the Health History Update and submit it in time to have it approved<br />
        <u>You may NOT practice until these three things are completed!</u></li>
        <li>New players - we need your jersey size and list of preferred numbers</li>
        <li>Seniors - please put your jerseys in my mailbox in the athletic office</li>
        <li>Exercises to start working on - shuffles, star drill, overhead swing w/ hand weight, set-to-self, set against wall</li>
    </ul>-->
        <br />
    </asp:Panel>
</asp:Content>

