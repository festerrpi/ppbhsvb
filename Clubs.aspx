﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Clubs.aspx.vb" Inherits="Clubs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Club Information
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="Panel1" runat="server"><b>Club Volleyball Information</b><br />
    <br />
    I have received information from the clubs below regarding playing opportunities.<br />
    <br />
    I have known all of the club directors for years, and am friendly to varying degrees with all of them, and can vouch for their volleyball knowledge and skill.<br />
    <br />
    I am putting these here for your information, in case you are interested in playing club volleyball.  I am not endorsing any club, nor saying that your participation is required or even suggested.<br />
    <br />
    Please let me know if you decide to attend/try out for any of these.  I like to know what's going on!<br />
    <br />
        <a href="Conquest_Tryout_Handout_2020.pdf" target="_blank">Conquest Volleyball</a><br />
        <a href="MOCO_Tryouts_2020.pdf" target="_blank">MOCO Volleyball</a><br />
        <a href="Six_Pack_Practice_Flyer.pdf" target="_blank">Warren Six Pack</a>
     
    </asp:Panel>
</asp:Content>

