﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ Register TagPrefix="My" TagName="UserInfoBoxControl" Src="~/wotd.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">PPBHSVB
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnlHeadline" runat="server">
        <b>For a Good Cause!</b><br /><br />
        Several of our players will be participating in a <a href="charitytournament.png" target="_blank">charity tournament</a> this weekend.  Good luck to all!<br /><br />
    <b>New players!</b><br />
        At the Celebrate the School event, we had 14 incoming freshmen express interest in joining the team.  14!<br /><br />
        <b>Start date for open gyms</b><br />
        Yet to be confirmed, but I'm looking at Tuesday, July 5th.  We will do 3 days per week, Monday, Tuesday, and Thursday, as usual, and probably first thing in the morning, as usual.<br /><br />
        <b>What a Season!</b><br />
        Thanks to the whole team, parents, staff, and supporters for a wonderful season.  I expected a good season, and what we got was a great one - this will be tough to beat.<br /><br />
        
        <b>Thanks, Chick-fil-A!</b><br />
        We had a good turnout for an impromptu team dinner at Chick-fil-A.  Thanks to everyone who participated.  We will definitely do this as a fundraiser again to get next season going.<br /><br />
       
        <!-- Spring 2021 <b>What a finish!</b><br /><br />
        Best match of the season.  Thanks to all of the players and parents for making this season what it was - which was great!<br />
        Enjoy the remainder of the school year... and we'll be back in the gym before you know it!<br />
        <a href="https://www.youtube.com/watch?v=CMbrCZqKmOc" target="_blank">Video</a> of the match against Trinity Hall.<br /><br />
        New players this season - please wash and return your jerseys to Coach asap.<br />-->
        Check back for updates on schedules, team events and news, and more great volleyball information. It&#39;s a work in progress.<br />
    <br />
    Questions or comments, please email Coach Hanniffy.<br />
    </asp:Panel>    
    <br />

    <asp:Panel ID="Panel3" runat="server">Word of the day: <My:UserInfoBoxControl runat="server" ID="WOTD" />
    </asp:Panel>
</asp:Content>