﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Documents.aspx.vb" Inherits="Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Documents and Forms
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
Volleyball stuff
<ul id="Docslist">
    <li><b>NEW</b> - <a href="docs/Wrestling_Volleyball.pdf" target="_blank">Spectator Policy for home matches</a></li>
    <li><b>NEW</b> - <a href="docs/Covid_Signoff.pdf" target="_blank">COVID Daily Sign Off</a></li>
<li><a href="docs/2020_Intake_Update.pdf">2020 Intake/Update Form</a> - please complete and send to Coach</li>
<li><a href="Positioning_Basics.pdf" target="_blank">Positioning Basics</a> - important to understand</li>
    <li><a href="docs/Fundraiser_Request.pdf" target="_blank">Fundraiser Request</a></li>
    <li><a href="docs/Travel_Release.pdf" target="_blank">Travel Release</a> - EVERYBODY please complete this (have a parent sign, then give to Coach)</li>
</ul>
Health stuff
<ul id="Formslist">
<li><a href="docs/HHQ.pdf">Health History Update Questionnaire</a></li>
<li><a href="docs/HHQ_Info.pdf">Updates to the Health History Update Questionnaire</a></li>
<li><a href="docs/COVID_questionnaire.pdf">COVID questionnaire</a></li>
<li><a href="docs/NJ_Health_-_Self-quarantine.pdf">Self-Quarantine for Travelers FAQ</a></li>
<li><a href="docs/NJSIAA_-_Covid_procedures.pdf">NJSIAA - Covid positive test procedures</a></li>
</ul>
</asp:Content>