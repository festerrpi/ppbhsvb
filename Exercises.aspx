﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.master" AutoEventWireup="false" CodeFile="Exercises.aspx.vb" Inherits="Exercises" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">Solo Exercises
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:scriptmanager ID="Scriptmanager1" runat="server"></asp:scriptmanager>
    <asp:Panel ID="Panel1" runat="server">Some exercises you can do on your own for strength and technique:
    </asp:Panel>

    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    <asp:BulletedList ID="BulletedList1" runat="server" OnClick="BulletedList1_Click" DisplayMode="LinkButton" >
    <asp:ListItem>Ankle strength (isometric)</asp:ListItem>
    <asp:ListItem>Ankle/calf strength</asp:ListItem>
    <asp:ListItem>Lateral ankle strength</asp:ListItem>
    <asp:ListItem>Broad jumps</asp:ListItem>
    <asp:ListItem>Broad hops</asp:ListItem>
    </asp:BulletedList>


    <asp:Panel runat="server" Visible="False" ID="PanelExercise0">Balance on one foot, other foot slightly raised, arms out - 15-30 seconds.<br />
    Now bring your arms in to your sides - 15-30 seconds.<br />
    Now close your eyes - 15-30 seconds.<br />
    Repeat for your other foot</asp:Panel>
    <asp:Panel ID="PanelExercise1" runat="server" Visible="False"><u>Part 1</u><br />Standing with feet shoulder width apart, Standing on one foot, slowly rise up onto your toes, then slowly back down. Now do the same motion, but more quickly on the way up. Now more quickly so you are beginning to jump.<br /><br />
    <u>Part 2</u><br />
    Standing on one foot, go through the same progression.  Switch legs and repeat the progression.<br />
    </asp:Panel>
    <asp:Panel ID="PanelExercise2" runat="server" Visible="False">Stand with your feet shoulder width apart.<br />
    Shift your weight back and forth from one foot to the other.<br />
    Gradually move your feet farther apart, so that you begin to jump from side to side.<br />
    </asp:Panel>
    <asp:Panel ID="PanelExercise3" runat="server" Visible="false">Broad jump.  Land evenly and stably with both feet.<br />
    <br />
    Broad jumps w/ pause.  Broad jump, hold the landing for two seconds, then jump again.<br />
    <br />
    Continuous jumps.  String 8 to 10 jumps together with a minimal pause (unless you start to lose balance)
    </asp:Panel>
    <asp:Panel ID="PanelExercise4" runat="server" Visible="false">Stand on one foot.  Warm up by lifting your heel off the ground a few times while you balance.<br />
    <br />
    Hop foward and land under control and balanced.  Repeat a few times to get the hang of it.<br />
    <br />
    Now string several together in a row, making sure you are balanced on each landing.
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

