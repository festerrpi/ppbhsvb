﻿
Partial Class Exercises
    Inherits System.Web.UI.Page

    Protected Sub BulletedList1_Click(sender As Object, e As BulletedListEventArgs)

        ' Dim ItemText As String = "PanelExercise" & e.Index
        Dim NumPanels As Integer = 4 ' one less than the number of entries
        Dim HidePanel As Object
        Dim ShowPanel As Object
        ShowPanel = sender.Parent.FindControl("PanelExercise" & e.Index)

        For i = 0 To NumPanels
            HidePanel = sender.Parent.FindControl("PanelExercise" & i)
            HidePanel.Visible = False
        Next

        ShowPanel.Visible = True

    End Sub
End Class
