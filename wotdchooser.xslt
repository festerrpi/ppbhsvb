﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="no" omit-xml-declaration="yes"/>
    <xsl:param name="date" />
  
    <xsl:template match="*">
      <u><xsl:value-of select="*[@date = $date]/@name" /></u> - <xsl:value-of select="*[@date = $date]/definition" />

<!--          <xsl:value-of select="*[@date = $date]/description" /> -->
<!--          <xsl:for-each select="*">
            <xsl:value-of select="@name" />
            <br />
          </xsl:for-each>
-->    </xsl:template>
</xsl:stylesheet>
