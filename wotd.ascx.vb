﻿Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports System.IO

Partial Class wotd
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs)
        Dim offset As Integer
        Dim startDate As Date = Date.Parse("07/31/2020")
        Dim currDate As Date = Now

        ' Determine the number of days between the two dates.
        offset = DateDiff(DateInterval.Day, startDate, currDate) Mod 36 + 1

        Dim strxmlfile As String = Server.MapPath("wotd.xml")
        Dim strxslfile As String = Server.MapPath("wotdchooser.xslt")
        Dim xPathDoc As XPathDocument = New XPathDocument(strxmlfile)
        Dim transform As XslCompiledTransform = New XslCompiledTransform
        Dim xslArgsList As XsltArgumentList = New XsltArgumentList()
        xslArgsList.AddParam("date", "", offset)
        Dim htmlOutput As New StringBuilder()
        Dim htmlWriter As StringWriter = New StringWriter(htmlOutput)
        transform.Load(strxslfile)
        transform.Transform(xPathDoc, xslArgsList, htmlWriter)
        WordLabel.Text = htmlOutput.ToString()
    End Sub

End Class
